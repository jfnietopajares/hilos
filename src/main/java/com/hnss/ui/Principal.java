package com.hnss.ui;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class Principal extends VerticalLayout {

	public Principal() {
		final HorizontalLayout layout = new HorizontalLayout();
		final HorizontalLayout izquirda = new HorizontalLayout();
		final HorizontalLayout derecha = new HorizontalLayout();
		layout.addComponents(izquirda, derecha);

		Button buttonAlb = new Button("Albaranes");
		buttonAlb.addClickListener(e -> {
			derecha.removeAllComponents();
			derecha.addComponent(new Label("Albaranes"));
		});

		Button buttonCorr = new Button("Correos");
		buttonCorr.addClickListener(e -> {
			derecha.removeAllComponents();
			derecha.addComponent(new Label("Correos"));
		});
		izquirda.addComponents(buttonAlb, buttonCorr);
		this.addComponent(layout);
	}

	public Principal(Component... children) {
		super(children);
		// TODO Auto-generated constructor stub
	}

}
